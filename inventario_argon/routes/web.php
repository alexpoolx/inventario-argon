<?php

use App\Http\Controllers\ArticuloController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\ResultsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/home', 'App\Http\Controllers\HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'App\Http\Controllers\UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'App\Http\Controllers\ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'App\Http\Controllers\ProfileController@update']);
	Route::get('upgrade', function () {return view('pages.upgrade');})->name('upgrade'); 
	 Route::get('map', function () {return view('pages.maps');})->name('map');
	 Route::get('icons', function () {return view('pages.icons');})->name('icons'); 
	 Route::get('table-list', function () {return view('pages.tables');})->name('table');
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'App\Http\Controllers\ProfileController@password']);
});

Route::get('/articulo',[ArticuloController::class, 'index'])->name('articulo.index');
Route::get('/articulo/create',[ArticuloController::class,'create'])->name('articulo.create');
Route::post('/articulo/store',[ArticuloController::class,'store'])->name('articulo.store');
Route::get('/articulo/{id}/',[ArticuloController::class,'show'])->name('articulo.show');
Route::get('/articulo/{id}/edit',[ArticuloController::class,'edit'])->name('articulo.edit');
Route::put('/articulo/update/{id}',[ArticuloController::class,'update'])->name('articulo.update');
Route::delete('/articulo/delete/{id}',[ArticuloController::class,'destroy'])->name('articulo.delete');

Route::post('/cart/add/',[CartController::class, 'add'])->name('cart-add');
Route::get('/cart/delete/{id}',[CartController::class, 'delete'])->name('cart-delete');
Route::get('/cart/trash',[CartController::class, 'trash'])->name('cart-trash');
Route::get('/details-cart',[CartController::class,'details'])->name('details-cart');

Route::get('/paypal/pay/',[PaymentController::class,'payWithPaypal'])->name('paypal.pay');
Route::get('/paypal/paypalStatus/',[PaymentController::class,'paypalStatus'])->name('paypal.status');
Route::get('/paypal/failed',[PaymentController::class,'paypalFailed'])->name('paypal.failed');
Route::get('/results',[ResultsController::class, 'results'])->name('results');