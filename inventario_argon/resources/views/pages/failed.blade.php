@extends('layouts.app')

@section('content')
@include('layouts.headers.cards')
    <div class="container-fluid mt--7">
   
        <div class="container-fluid mt--7">
           @if(session('status'))
            <div class="alert alert-danger d-flex justify-center" role="alert">
                {{session('status')}}
            </div>
            <a href="{{route('articulo.index')}}"  class="mt-7 btn btn-sm btn-success">Regresar</a>
           @endif
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush