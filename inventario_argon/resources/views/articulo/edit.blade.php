@extends('layouts.app')

@section('content')
@include('layouts.headers.cards')
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-4 order-xl-2 mb-5 mb-xl-0">
                <div class="card card-profile shadow">
                    <div class="row justify-content-center">
                        <div class="col-lg-3 order-lg-2">
                            <div class="card-profile-image">
                                <a href="#">
                                    <img src="{{ asset('argon') }}/img/theme/team-4-800x800.jpg" class="rounded-circle">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
                        <div class="d-flex justify-content-between">
                            <a href="#" class="btn btn-sm btn-info mr-4">{{ __('Connect') }}</a>
                            <a href="#" class="btn btn-sm btn-default float-right">{{ __('Message') }}</a>
                        </div>
                    </div>
                    <div class="card-body pt-0 pt-md-4">
                        <div class="row">
                            <div class="col">
                                <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                                    <div>
                                        <span class="heading">22</span>
                                        <span class="description">{{ __('Friends') }}</span>
                                    </div>
                                    <div>
                                        <span class="heading">10</span>
                                        <span class="description">{{ __('Photos') }}</span>
                                    </div>
                                    <div>
                                        <span class="heading">89</span>
                                        <span class="description">{{ __('Comments') }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="text-center">
                            <h3>
                                {{ auth()->user()->name }}<span class="font-weight-light">, 27</span>
                            </h3>
                            <div class="h5 font-weight-300">
                                <i class="ni location_pin mr-2"></i>{{ __('Bucharest, Romania') }}
                            </div>
                            <div class="h5 mt-4">
                                <i class="ni business_briefcase-24 mr-2"></i>{{ __('Solution Manager - Creative Tim Officer') }}
                            </div>
                            <div>
                                <i class="ni education_hat mr-2"></i>{{ __('University of Computer Science') }}
                            </div>
                            <hr class="my-4" />
                            <p>{{ __('Ryan — the name taken by Melbourne-raised, Brooklyn-based Nick Murphy — writes, performs and records all of his own music.') }}</p>
                            <a href="#">{{ __('Show more') }}</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-8 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <h3 class="mb-0">Editar Artículo</h3>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('articulo.update',$articulo->id) }}" autocomplete="off" enctype="multipart/form-data">
                            {{csrf_field()}}
                            @method('PUT')
                            <h6 class="heading-small text-muted mb-4">Información del Artículo</h6>
                            
                            @if (session('status'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {{ session('status') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif


                            <div class="pl-lg-4">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="form-control-label" for="input-name">Código del Artículo</label>
                                        <input type="text" name="codarticulo" value="{{$articulo->codarticulo}}" id="input-name" class="form-control form-control-alternative">
                                    </div>
    
                                    <div class="col-md-6">
                                        <label class="form-control-label" for="input-name">Nombre</label>
                                        <input type="text" name="nombre" value="{{$articulo->nombre}}" id="input-name" class="form-control form-control-alternative">
                                    </div>  

                                    <div class="col-md-6">
                                        <label class="form-control-label" for="input-name">Precio</label>
                                        <input type="number" name="precio" value="{{$articulo->precio}}" id="input-name" class="form-control form-control-alternative">
                                    </div>  

                                    <div class="col-md-6">
                                        <label class="form-control-label" for="input-name">Stock</label>
                                        <input type="text" name="stock" value="{{$articulo->stock}}" id="input-name" class="form-control form-control-alternative">
                                    </div>
                                    <div class="col-md-6 mt-4">
                                        <img src="{{$articulo->imagen}}" alt="" width="300" height="300" class="rounded">                                  
                                    </div>
                                    <div class="col-md-6">
                                        <label class="form-control-label" for="input-name">Imagen</label>                                
                                        <input type="file" class="form-control" name="imagen" id="input-name">
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        

        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush