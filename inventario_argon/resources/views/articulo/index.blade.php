@extends('layouts.app')

@section('content')
@include('layouts.headers.cards')
    <div class="container-fluid mt--7">
   
        <div class="row mt-5">
            <div class="col-xl-10 mb-5 mb-xl-0">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Artículos</h3>
                            </div>
                            <form class="navbar-search form-inline mr-3 d-none d-md-flex" method="GET" action="{{route('articulo.index')}}">
                                <div class="form-group mb-0">
                                    <div class="input-group input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><button type="submit" class="btn btn-success btn-sm"><i class="fas fa-search"></i></button></span>
                                        </div>
                                        <input class="form-control" name="search" placeholder="Search" type="text">
                                    </div>
                                </div>
                            </form>
                            <div class="col text-right">
                                <a href="{{route('details-cart')}}" class="btn btn-sm btn-primary"><i class="ni ni-cart"></i> Ir al carrito</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-12">
                        <div class="row">
                            @if(count($articulos) == 0)
                                <div class="alert alert-warning d-flex justify-center" role="alert">
                                    No se encontraron resultados   
                                </div>
                                @else
                            @foreach($articulos as $art)
                            <div class="col-md-4 mb-3">
                                <div class="card " style="width: 18rem;">
                                    <img class="card-img-top" src="{{$art->imagen}}" alt="Card image cap" width="100" height="200">
                                    <div class="card-body">
                                    <h3 class="card-title">{{$art->nombre}}</h3>
                                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                        <p class="bg-gradient-primary text-white text-center"><span class="font-weight-bold">precio: </span>{{$art->precio}}$</p>
                                    </div>
                                    <div class="card-footer text-center bg-dark">
                                        <a href="{{route('articulo.show',$art->id)}}" class="btn btn-primary btn-sm" title="Ver"><i style="color: #ffffff;" class="ni ni-zoom-split-in"></i></a>
                                        <a  class="btn btn-primary btn-sm"  data-toggle="modal" data-target="#exampleModal{{$art->id}}" title="Añadir al carrito" ><i style="color: #9AE66E;" class="ni ni-cart"></i></a>
                                    </div>
                                </div>
                            </div>

                            <!-- Modal -->
                            <div class="modal fade" id="exampleModal{{$art->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <h2 class="modal-title" id="exampleModalLabel">Detalles del Artículo</h2>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p class="card-title font-weight-bold">Nombre: {{$art->nombre}}</p>
                                                <p class="card-text font-weight-bold">Stock: {{$art->stock}}</p>                                              
                                                <p class="text-warning"><span class="font-weight-bold">Precio: </span>{{$art->precio}}$</p>
                                                
                                                    <form action="{{route('cart-add')}}" method="post">
                                                        {{csrf_field()}}
                                                        <div class="row">
                                                            <div class="col-md-5">
                                                                
                                                                <input type="text" value="{{$art->id}}" hidden name="id">
                                                                <input type="number" class="form-control" value="1" min="1" name="cantidad">  
                                                            </div>  
                                                            <button type="submit" class="btn btn-success">+</button>
                                                        </div>
                                                    </form>       
                                                
                                            </div>
                                            <div class="col-md-6">
                                                <img class="card-img-top" src="{{$art->imagen}}" alt="Card image cap" width="200" height="200">
                                            </div>
                                        </div>                                   
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>                                    </div>
                                </div>
                                </div>
                            </div>

                            @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush