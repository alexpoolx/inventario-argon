<?php

namespace App\Http\Controllers;

use App\Models\Articulo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ArticuloController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = trim($request->get('search'));
        if(!$search){
            $articulos = Articulo::paginate(5);
            return view('articulo.index',compact('articulos','search'));
        }else{
                $articulos = DB::table('articulos')
                ->select('id','codarticulo','nombre','precio','stock','imagen')
                ->where('codarticulo','=',$search)
                ->paginate(5);
            return view('articulo.index',compact('articulos','search'));
        }

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('articulo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $articulo = new Articulo();

       $articulo->codarticulo = $request->get('codarticulo');
       $articulo->nombre = $request->get('nombre');
       $articulo->precio = $request->get('precio');
       $articulo->stock = $request->get('stock');
       
       if($request->hasFile('imagen')){
        $uploadSuccess = $request->file('imagen')->store('public/images/articulos');
        $url_imagen = Storage::url($uploadSuccess);
        $articulo->imagen = $url_imagen;
       }
   
       $articulo->save();

       return redirect()->route('articulo.index');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $total = session('total');
        $articulo = Articulo::find($id);
        return view('articulo.show',compact('articulo','total'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $articulo = Articulo::findOrFail($id);
        return view('articulo.edit',compact('articulo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
       $articulo = Articulo::findOrFail($id);
       $articulo->codarticulo = $request->get('codarticulo');
       $articulo->nombre = $request->get('nombre');
       $articulo->precio = $request->get('precio');
       $articulo->stock = $request->get('stock');
       
       if($request->hasFile('imagen')){
        $url = str_replace("storage","public",$articulo->imagen);
        Storage::delete($url);
        $uploadSuccess = $request->file('imagen')->store('public/images/articulos');
        $url_imagen = Storage::url($uploadSuccess);
        $articulo->imagen = $url_imagen;
       }
   
       $articulo->save();
       
       return redirect()->route('articulo.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $articulo = Articulo::find($id);
        $url = str_replace("storage","public",$articulo->imagen);
        Storage::delete($url);
        $articulo->delete();
        return redirect()->route('articulo.index');
    }
}
