<?php

namespace App\Http\Controllers;

use App\Models\Articulo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Payer;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Amount;
use PayPal\Api\Transaction;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Payment;
use PayPal\Api\Details;
use PayPal\Api\PaymentExecution;
use PayPal\Exception\PayPalConnectionException;
use Illuminate\Contracts\Session\Session;


class PaymentController extends Controller
{   
    private $apiContext;
    
    public function __construct()
    {   
        
        $payPalConfig = Config::get('paypal');
        $this->apiContext = new ApiContext(
            new OAuthTokenCredential(
                $payPalConfig['client_id'],
                $payPalConfig['secret']
            )
        );
    }

    public function payWithPaypal(){
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        $currency = 'USD';
        $articulo = session('cart');
        $total = session('total');
        foreach($articulo as $art){
            $item = new Item();
            $item->setName($art->nombre)
                     ->setCurrency($currency)
                     ->setQuantity($art->cantidad)
                     ->setPrice($art->precio);
            $items[] = $item;
        }


        $item_list = new ItemList();
        $item_list->setItems($items);

        $detalle = new Details();
        $detalle->setSubtotal($total)
                ->setShipping(0);

        $amount = new Amount();
        $amount->setCurrency("USD")
                ->setDetails($detalle)
                ->setTotal($total);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
                    ->setItemList($item_list)
                    ->setDescription('Pedido de prueba en mi App Store');

        $callBackUrl = url('/paypal/paypalStatus/');

        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl($callBackUrl)
                     ->setCancelUrl($callBackUrl);

        $payment = new Payment();
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setTransactions(array($transaction))
            ->setRedirectUrls($redirectUrls);    

            try {
                $payment->create($this->apiContext);
                return redirect()->away($payment->getApprovalLink());
            } catch (PayPalConnectionException $ex) {
                echo $ex->getData();
            }    

    }

    public function paypalStatus(Request $request){
        $paymentId = $request->get('paymentId');
        $token = $request->get('token');
        $PayerID = $request->get('PayerID');
        if (!$paymentId || !$PayerID || !$token) {
            $status = 'Lo sentimos! El pago a través de PayPal no se pudo realizar.';
            return redirect('/paypal/failed')->with(compact('status'));
        }

        $payment = Payment::get($paymentId,$this->apiContext);
        $execution = new PaymentExecution();
        $execution->setPayerId($PayerID);

        $result = $payment->execute($execution,$this->apiContext);
        
        if($result->getState() == 'approved'){
            $status = 'Gracias! El pago a través de PayPal se ha ralizado correctamente.';
            session()->forget('cart');
            session()->forget('total');
            return redirect('results')->with(compact('status'));
        }
            
        $status = 'Lo sentimos! El pago a través de PayPal no se pudo realizar.';
        return redirect('results')->with(compact('status'));
        
    }


    public function paypalFailed(){
        return view('pages.failed');
    }
}
