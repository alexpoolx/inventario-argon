<?php

namespace App\Http\Controllers;

use App\Models\Articulo;
use Illuminate\Http\Request;
use Session;
class CartController extends Controller
{
    public function __construct()
    {
        if(!session()->has('cart'))
            session()->put('cart',array());
    }

    public function add(Request $request){
       $articulo = Articulo::find($request->get('id'));
       $cart = session('cart');
       $carrito = [];
       $carrito['id'] = $articulo->id;
       $carrito['nombre'] = $articulo->nombre;
       $carrito['precio'] = $articulo->precio;
       $carrito['stock'] = $articulo->stock;
       $carrito['cantidad'] = $request->get('cantidad');    
       $cart[] = $carrito;
       session(['cart' => $cart]); 
      return redirect()->route('articulo.index');
    }

    public function getTotal(){
        $cart =  session()->get('cart');
        $total = 0;
            foreach($cart as $item){
                $total = $total + $item['precio'] * $item['cantidad'];
            }      
        return $total;
    }

    public function delete($item){
        $temp = [];
        $cart = session('cart');
        unset($cart[$item]);
        $temp = $cart;
        session()->put('cart',$temp);
        return redirect()->route('details-cart');
    }

    public function trash(){
        session()->forget('cart');       
        return redirect()->route('articulo.index','total');
    }

    public function details(){
        $status = '';
        if(count(session()->get('cart'))==0){
            $status = 'Aún no posee articulos en el carrito';
            return view('shopping-cart.details-cart')->with(compact('status'));
        }
        $cart = session('cart');
        $total = $this->getTotal();
        return view('shopping-cart.details-cart')->with(compact('cart','total','status'));
    }

}
